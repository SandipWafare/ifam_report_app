﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RDLToRDLC.RDLCReportModel
{
    public class SysReportDirectory
    {
        static string connStr = System.Configuration.ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;

        public static DataSet GetSysReportDirectory(int ReportID)
        {
            DataSet Ds = new DataSet();

            SqlConnection con = new SqlConnection(connStr);
            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                SqlCommand cmd = new SqlCommand("[dbo].[SelectReportData]", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ReportID", ReportID);             

                DataSet ds = new DataSet();
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(ds);
                return ds;
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                con.Close();
            }

            return null;
        }
    }
}