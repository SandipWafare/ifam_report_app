﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RDLToRDLC.RDLCReportModel
{
    public static class STM48_ASSETBLOCK_WISE
    {

        static string connStr = System.Configuration.ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;

        public static DataSet getSTM48_ASSETBLOCK_WISE(string AssetBlock, int TIMEKEY)
        {
            DataSet Ds = new DataSet();

            SqlConnection con = new SqlConnection(connStr);
            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                SqlCommand cmd = new SqlCommand("[dbo].[SMT48Report_BlockWise]", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@AssetBlock", AssetBlock);
                cmd.Parameters.AddWithValue("@TIMEKEY", TIMEKEY);

                DataSet ds = new DataSet();
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(ds);
                return ds;
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                con.Close();
            }

            return null;
        }

    }
}