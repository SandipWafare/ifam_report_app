﻿using Microsoft.Reporting.WebForms;
using RDLToRDLC.RDLCReportModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RDLToRDLC
{
    public partial class Default : System.Web.UI.Page
    {
        DataSet Ds;
        int strReportMenuId;
        List<ReportParameter> lstParamter = new List<ReportParameter>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Ds = new DataSet();
                try
                {
                    strReportMenuId = int.Parse(Request.QueryString["ReportID"]);
                    Ds = SysReportDirectory.GetSysReportDirectory(strReportMenuId);

                    string[] ParamList = Convert.ToString(Ds.Tables[0].Rows[0]["ReportParams"]).Split(',');

                    foreach (string row in ParamList)
                    {
                        string StrData = Convert.ToString(Request.QueryString[row]);
                        lstParamter.Add(new ReportParameter(row, StrData, false));
                    }
                }
                catch (Exception ex)
                {

                }
                if (Ds.Tables.Count > 0)
                    if (Ds.Tables[0].Rows.Count > 0)
                    {

                        ReportViewer1.LocalReport.ReportPath = Convert.ToString(Ds.Tables[0].Rows[0]["ReportPath"]);
                        string Typename = "RDLToRDLC.RDLC_DataSrcTableAdapters." + Convert.ToString(Ds.Tables[0].Rows[0]["ReportTableAdapterName"]);
                        string SelectMethod = Convert.ToString(Ds.Tables[0].Rows[0]["SelectMethod"]);
                        ObjectDataSource _ObjectDataSource = new ObjectDataSource(Typename, SelectMethod);
                        _ObjectDataSource.ID = "ObjectDataSource1";
                        _ObjectDataSource.SelectParameters.Clear();

                        foreach (var item in lstParamter)
                        {
                            var RptParam = (ReportParameter)item;
                            if (RptParam.Name != "userLoginId")
                            {
                                if (strReportMenuId == 1100)
                                {
                                    if (RptParam.Name != "TIMEKEY")
                                        _ObjectDataSource.SelectParameters.Add(RptParam.Name, Convert.ToString(RptParam.Values[0]));
                                }
                                else
                                    _ObjectDataSource.SelectParameters.Add(RptParam.Name, Convert.ToString(RptParam.Values[0]));

                            }

                        }

                        ReportDataSource RDC = new ReportDataSource("DataSet1", _ObjectDataSource);
                        ReportViewer1.LocalReport.DataSources.Add(RDC);
                        ReportViewer1.LocalReport.SetParameters(lstParamter);
                        ReportViewer1.LocalReport.Refresh();
                    }
            }



        }
    }
}